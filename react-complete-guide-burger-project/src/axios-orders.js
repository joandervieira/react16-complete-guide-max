import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-1b7d8.firebaseio.com/'
});


export default instance;