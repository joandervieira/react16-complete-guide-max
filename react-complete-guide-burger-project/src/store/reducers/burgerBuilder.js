import * as actionsTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';


const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
};

const INGREDIENTS_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionsTypes.ADD_INGREDIENT: return addIngredient(state, action);
        case actionsTypes.REMOVE_INGREDIENT: return removeIngredient(state, action);
        case actionsTypes.SET_INGREDIENTS: return setIngredient(state, action);
        case actionsTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientFailed(state, action);
        default: return state;

    }
}

const addIngredient = (state, action) => {
    const updatedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingredientName],
        building: true
    };
    return updateObject(state, updatedState);
}

const removeIngredient = (state, action) => {
    const updatedIngr = { [action.ingredientName]: state.ingredients[action.ingredientName] - 1 };
    const updatedIngrs = updateObject(state.ingredients, updatedIngr);
    const updatedSt = {
        ingredients: updatedIngrs,
        totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingredientName]
    };
    return updateObject(state, updatedSt);
}

const setIngredient = (state, action) => {
    return updateObject(state, {
        ingredients: {
            salad: action.ingredients.salad,
            bacon: action.ingredients.bacon,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat
        },
        totalPrice: 4,
        error: false,
        building: false
    });
}

const fetchIngredientFailed = (state, action) => {
    return updateObject(state, {
        error: true
    });
}



export default reducer;