import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';

import axios from '../../axios-orders';
import Aux from "../../hoc/Aux";
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';



export class BurgerBuilder extends Component {

  state = {
    purchasing: false
  };

  componentDidMount() {
    this.props.onInitIngredients();
  }


  updatePurchaseState(updatedIngredients) {
    const ingredients = {
      ...updatedIngredients
    };

    const sum = Object.keys(ingredients)
      .map(key => {
        return ingredients[key];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);

    return sum > 0;
  }

  purchaseHandler = () => {
    if (this.props.isAuthenticated) {
      this.setState({ purchasing: true });
    } else {
      this.props.onSetRedirectPath('/checkout');
      this.props.history.push('/auth');
    }
  }

  purchaseCancelHandler = () => {
    this.setState({ purchasing: false });
  }

  purchaseContinueHandler = () => {
    this.props.onInitPurchase();
    this.props.history.push('/checkout');
  }

  render() {
    const disabledInfo = {
      ...this.props.ings
    };

    let orderSummary = null;

    let burger = <Spinner />;

    if (this.props.error) {
      burger = <p>Ingredients can't be loaded!</p>
    }

    if (this.props.ings) {
      burger = (
        <Fragment>
          <Burger ingredients={this.props.ings} />
          <BuildControls
            ingredientAdded={this.props.onIngredientAdded}
            ingredientRemoved={this.props.onIngredientRemoved}
            disabled={disabledInfo}
            price={this.props.totalPrice}
            purchaseable={this.updatePurchaseState(this.props.ings)}
            isAuth={this.props.isAuthenticated}
            ordered={this.purchaseHandler} />
        </ Fragment>
      );
      orderSummary = (
        <OrderSummary
          ingredients={this.props.ings}
          purchaseCanceled={this.purchaseCancelHandler}
          purchedContinued={this.purchaseContinueHandler}
          price={this.props.totalPrice}
        />
      );
    };


    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }

    return (
      <Aux>
        <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler} >
          {orderSummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    totalPrice: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    isAuthenticated: state.auth.token !== null
  }
}

const mapDispachToProps = dispach => {
  return {
    onIngredientAdded: (ingName) => dispach(actions.addIngredient(ingName)),
    onIngredientRemoved: (ingName) => dispach(actions.removeIngredient(ingName)),
    onInitIngredients: () => dispach(actions.initIngredients()),
    onInitPurchase: () => dispach(actions.purchaseInit()),
    onSetRedirectPath: (path) => dispach(actions.setAuthRedirectPath(path))
  }
};

export default connect(mapStateToProps, mapDispachToProps)(withErrorHandler(BurgerBuilder, axios));
