import React, { Component } from "react";

import Aux from "../../hoc/Aux";
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';
import { connect } from 'react-redux';



import classes from "./Layout.css";

class Layout extends Component {

  state = {
    showSideDrawer: false
  };

  sideDrawerCloseHandler = () => {
    this.setState({
      showSideDrawer: false
    });
  }

  drawerToggleHandler = () => {
    this.setState((prevState) => {
      return { showSideDrawer: !prevState.showSideDrawer }
    })
  }

  render() {

    return (
      <Aux>
        <Toolbar
          drawerToggleClicked={this.drawerToggleHandler}
          isAuth={this.props.isAuthenticated} />

        <SideDrawer
          open={this.state.showSideDrawer}
          closed={this.sideDrawerCloseHandler}
          isAuth={this.props.isAuthenticated} />

        <main className={classes.Content}>{this.props.children}</main>

      </Aux>
    );

  };
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(Layout);
